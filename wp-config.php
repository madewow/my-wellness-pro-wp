<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mwp_wp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '@*[qus5~u><nZCjlTzYX1v ErSL`#P%ArI.%^D@|d[qHPQk9/@:mS[]*M6 V-JPo');
define('SECURE_AUTH_KEY',  '5.:GOs]_QAtlRv|YC&SU$N?#Z(q!={n1,A1Y%j0EMqwOEwcW9jr5K=r?WprQmLXp');
define('LOGGED_IN_KEY',    '_dw6AGrKutDWRLjwoc=m{qX`hGG3x]HggG o<`D~{Z>VsbCAx3Sx)$/~kRsH:nq2');
define('NONCE_KEY',        'T=unr_ias/g[nKYBU<66gadTRX2iOV;Lr|k>x4^`}p+@$**9~&vZOnQ/!YwN~|#9');
define('AUTH_SALT',        'Z~{ G.nlU?{`S?S#t=?Q{Qk<Q,*d%VSY_2Tl4RK/5Se567Ac4z`0A(OIWL$Sh764');
define('SECURE_AUTH_SALT', '#ECh,?SBB! e6k27,KzC;EH+q4~Hfg$P*i,!V^wlwjnfLQD{s=KVESZZm^u5VZov');
define('LOGGED_IN_SALT',   ';$r5Y$^&qAKzrx#65Y(`Br~A9<G_I85uUVcz4VHy+<}4Db$BYOLrujrm )*MLrko');
define('NONCE_SALT',       ':KseTpK>=P,&%c4(n`<7Ok]^)/Z!23G;W,6lYmaRU`bv1`a],ZG0dvxA7lBjB_)_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
