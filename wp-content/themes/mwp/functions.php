<?php
/**
 * functions.php
 *
 */
 
function theme_styles() { 
	wp_enqueue_style( 'foundation-css', get_stylesheet_directory_uri() . '/css/foundation.css' );
	
	wp_enqueue_style( 'font-awesome-css', get_stylesheet_directory_uri() . '/css/font-awesome.min.css' );
	
	wp_enqueue_style( 'app-css', get_stylesheet_directory_uri() . '/css/app.css' );
}
add_action('wp_enqueue_scripts', 'theme_styles');

function theme_scripts() {
	wp_register_script( 'google-map-js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDKJIH7Ywy7JzOcsm9NhbpiRnCraSx5Dzk', array('jquery'), '1', true );
	
	wp_register_script( 'jquery-js', get_stylesheet_directory_uri() . '/js/vendor/jquery.js', array('jquery'), '1', true );
	
	wp_register_script( 'what-input-js', get_stylesheet_directory_uri() . '/js/vendor/what-input.js', array('jquery'), '1', true );
	
	wp_register_script( 'foundation-js', get_stylesheet_directory_uri() . '/js/vendor/foundation.js', array('jquery'), '1', true );
	
	wp_register_script( 'app-js', get_stylesheet_directory_uri() . '/js/app.js', array('jquery'), '1', true );
	
	wp_enqueue_script('google-map-js');
	wp_enqueue_script('jquery-js');
	wp_enqueue_script('what-input-js');
	wp_enqueue_script('foundation-js');
	wp_enqueue_script('app-js');
}

add_action( 'wp_enqueue_scripts', 'theme_scripts' ); 