<?php
	/**
	* Template Name: Plans & Pricing Template
	* Description: Template used for the home page
	*/
?>

<?php get_header(); ?>

		<section>
			<header class="header-small" data-interchange="[<?php bloginfo('stylesheet_directory'); ?>/img/header-home.jpg, small]">
				
			</header>
		</section>
		
		<section>
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Why Join MWP?</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="small-12 small-offseet-0 medium-10 medium-offset-1">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis consequat lorem sed nulla vehicula auctor. In vitae sem sem. Donec efficitur, nunc at viverra dictum, nisi ipsum vestibulum odio, ut pulvinar tellus nisi ac leo. Donec eros justo, mollis vitae nisi sed, condimentum feugiat mi. Vivamus hendrerit urna sed mauris feugiat, tristique facilisis ipsum vestibulum. Donec a leo mauris. Sed cursus purus faucibus, fringilla magna sed, interdum sem. Vivamus gravida ultrices leo ac euismod.</p>

						<p>In vulputate, metus ac porta laoreet, odio nunc elementum lorem, a vehicula enim nibh non nisi. Aenean accumsan neque vitae vehicula viverra. Nulla vel quam ac arcu interdum laoreet at ac tellus. Nullam semper odio metus, quis vehicula mi lobortis non. Curabitur rhoncus molestie elit porta tempus. Mauris lacus justo, facilisis sit amet iaculis quis, placerat non turpis. Fusce tortor massa, facilisis sodales tellus quis, cursus vestibulum massa. Pellentesque finibus purus metus, in congue orci facilisis sit amet. Mauris gravida arcu in risus convallis, tincidunt porta mauris sodales. Vestibulum ut interdum nisl. Integer vitae mollis sapien. Vivamus venenatis velit dolor, ut consectetur sapien egestas quis. Vivamus et ultrices nisl. Nullam euismod quam nec porttitor pulvinar.</p>
				</div>
			</div>
		</section>
		
		<section id="plans-prices">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Plans &amp; Prices</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row small-up-1 medium-up-3" data-equalizer data-equalize-on="medium">
				<div class="columns column-block">
					<div class="plans">
						<h3 class="text-center">Free</h3>
						
						<h1 class="text-center">$0</h1>
						
						<ul data-equalizer-watch>
							<li>Text only option</li>
							<li>Basic listing</li>
							<li>Bio</li>
							<li>Website & social media links</li>
						</ul>
						
						<div class="text-center">
							<a class="button">Sign me up!</a>
						</div>
					</div>
				</div>
				
				<div class="columns column-block">
					<div class="plans highlight">
						<h3 class="text-center">Platinum</h3>
						
						<h1 class="text-center">$49.99 <sub>/ month</sub></h1>
						
						<ul data-equalizer-watch>
							<li>Text, image &amp; video option</li>
							<li>Customizable image background</li>
							<li>Membership/access to biz courses</li>
							<li>How to's Link/integrate with appt booking</li>
							<li>Blogging capability (with admin edit/approval by us)</li>
							<li>Reviews/star rating</li>
							<li>Website (with tutorials)</li>
							<li>Migration from other wordpress sites</li>
						</ul>
						
						<div class="text-center">
							<a class="button">Sign me up!</a>
						</div>
					</div>
				</div>
				
				<div class="columns column-blocks">
					<div class="plans">
						<h3 class="text-center">Gold</h3>
						
						<h1 class="text-center">$19.99 <sub>/ month</sub></h1>
						
						<ul data-equalizer-watch>
							<li>Text, image & video option</li>
							<li>Customizable image background</li>
							<li>Membership/access to biz courses</li>
							<li>How to's Link/integrate with appt booking</li>
							<li>Blogging capability (with admin edit/approval by us)</li>
							<li>Reviews/star rating</li>
						</ul>
						
						<div class="text-center">
							<a class="button">Sign me up!</a>
						</div>
					</div>
				</div>
			</div>
		</section>
		
<?php get_footer(); ?>