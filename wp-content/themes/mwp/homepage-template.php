<?php
	/**
	* Template Name: Home Page Template
	* Description: Template used for the home page
	*/
?>

<?php get_header(); ?>

		<section id="home-header">
			<header class="header-large" data-interchange="[<?php bloginfo('stylesheet_directory'); ?>/img/header-home.jpg, small]">
				<div class="container">
					<div class="row">
						<div class="columns small-12 small-offset-0 large-8 large-offset-2">
							<h1 class="text-center">Just the right Practitioner</h1>
							
							<form id="home-search">
								<div class="row">
									<div class="columns small-12">
										<input type="text" placeholder="Your location, address, region, city, postal code">
										
										<a class="btn-location"><i class="fa fa-map-o"></i></a>
									</div>
								</div>
								
								<div class="row">
									<div class="columns small-12 large-6">
										<select>
											<option>Profession</option>
										</select>
									</div>
									
									<div class="columns small-12 large-6">
										<select>
											<option>Condition</option>
										</select>
									</div>
								</div>
								
								<div class="row">
									<div class="columns small-12 text-center">
										<button><i class="fa fa-search fa-2x"></i></button>	
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</header>
		</section>
		
		<section id="home-intro">
			<div class="row">
				<div class="columns small-12 small-offset-0 large-10 large-offset-1">
					<p>We know how hard it is to find ‘just the right’ practitioner for your needs.   With that in mind, we have created a place for you to learn about the modalities available to you in the alternative, holistic, and wellness industry.    There are a lot!  We have broken it down into bite size chunks, and have a search tool to help you find the practitioner that is right for you.</p>
					
					<p>You can search by location, modality, and name while also being able to see how the practitioner is rated.   Our quick glance seal will tell you if they are verified meaning they have submitted proof of education and professional licensure or registration.   This will help you narrow down someone who is qualified to work with you.</p>
				</div>
			</div>
		</section>
		
		<section id="home-for-practitioners">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>For Practitioners</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12 small-offset-0 medium-10 medium-offset-1">
					<p>Building a successful practice is not easy. It requires hard work, dedication, business skills, and of course a kick ass website. You also need to be found. If people don’t know you exist, they can’t benefit from all of your amazing, life changing recommendations. </p>

					<p>Here you will find solutions to all aspects of your business needs. Exciting right?</p>
				</div>
			</div>
			
			<div class="row">
				<div class="columns small-12 medium-4">
					<a class="icon-for-practitioners">
						<i class="fa fa-heartbeat fa-2x"></i>
					</a>
					
					<h3>Lorem Ipsum</h3>
						
					<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
				</div>
				
				<div class="columns small-12 medium-4">
					<a class="icon-for-practitioners">
						<i class="fa fa-heartbeat fa-2x"></i>
					</a>
					
					<h3>Lorem Ipsum</h3>
						
					<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
				</div>
				
				<div class="columns small-12 medium-4">
					<a class="icon-for-practitioners">
						<i class="fa fa-heartbeat fa-2x"></i>
					</a>
					
					<h3>Lorem Ipsum</h3>
						
					<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
				</div>
			</div>
		</section>
		
		<section id="home-recommended-reading">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Recommended Reading</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="recommended-article">
				<div class="row">
					<div class="columns small-12 large-7 large-offset-1">
						<h3><a>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a></h3>
						
						<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
					</div>
					
					<div class="columns small-12 large-3 end hide-for-small-only">
						<div class="thumb">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_article.jpg">
							
							<a>
								<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
							</a>	
						</div>
					</div>
				</div>
				
				<div class="row">
					<div class="columns small-12 large-3 large-offset-1 hide-for-small-only">
						<div class="thumb">
							<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_article.jpg">
							
							<a>
								<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
							</a>	
						</div>
					</div>
					
					<div class="columns small-12 large-7 end">
						<h3><a>Lorem ipsum dolor sit amet, consectetuer adipiscing elit</a></h3>
						
						<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. </p>
					</div>
				</div>
			</div>
		</section>

<?php get_footer(); ?>