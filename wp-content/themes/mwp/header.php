<?php
?><!doctype html>
<html class="no-js" lang="en" dir="ltr">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="x-ua-compatible" content="ie=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<?php wp_head(); ?>
	</head>
	
	<body>
		<nav id="main-navigation">
			<div class="row">
				<div class="columns small-12 medium-6">
					<a class="logo-text"><img src="<?php bloginfo('stylesheet_directory'); ?>/img/logo-text.png"></a>
				</div>
				
				<div class="columns small-12 medium-6">
					<a class="trigger-menu">
						<span></span>
					</a>
					
					<ul>
						<li>
							<a>PRACTITIONER</a>
						</li>
					</ul>
				</div>
			</div>
			
			<a class="logo-mark"></a>
		</nav>