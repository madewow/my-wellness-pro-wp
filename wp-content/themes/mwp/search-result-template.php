<?php
	/**
	* Template Name: Search Result Template
	* Description: Template used for the home page
	*/
?>

<?php get_header(); ?>

		<section>
			<header class="header-large" id="map">
				
			</header>
		</section>
		
		<section class="search-result-container">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>Within 10 km</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="small-12 small-offseet-0 medium-10 medium-offset-1">
					<div class="row">
						<div class="columns small-12 medium-6">
							<div class="search-result">
								<div>
									<div class="thumb">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
									</div>
								</div>
								
								<div>
									<h4>Jane Doe</h4>
									
									<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
									
									<ul id="practitioner-action">
										<li>
											<div><i class="fa fa-envelope"></i></div>
											
											<a>Message Practitioner&nbsp;&nbsp;&nbsp;</a>
										</li>
										
										<li>
											<div><i class="fa fa-bookmark"></i></div>
											
											<a>Bookmark</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						
						<div class="columns small-12 medium-6">
							<div class="search-result">
								<div>
									<div class="thumb">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
									</div>
								</div>
								
								<div>
									<h4>Jane Doe</h4>
									
									<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
									
									<ul id="practitioner-action">
										<li>
											<div><i class="fa fa-envelope"></i></div>
											
											<a>Message Practitioner&nbsp;&nbsp;&nbsp;</a>
										</li>
										
										<li>
											<div><i class="fa fa-bookmark"></i></div>
											
											<a>Bookmark</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="small-12 small-offseet-0 medium-10 medium-offset-1">
					<div class="row">
						<div class="columns small-12 medium-6">
							<div class="search-result">
								<div>
									<div class="thumb">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
									</div>
								</div>
								
								<div>
									<h4>Jane Doe</h4>
									
									<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
									
									<ul id="practitioner-action">
										<li>
											<div><i class="fa fa-envelope"></i></div>
											
											<a>Message Practitioner&nbsp;&nbsp;&nbsp;</a>
										</li>
										
										<li>
											<div><i class="fa fa-bookmark"></i></div>
											
											<a>Bookmark</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						
						<div class="columns small-12 medium-6">
							<div class="search-result">
								<div>
									<div class="thumb">
										<img src="<?php bloginfo('stylesheet_directory'); ?>/img/tn_profile.jpg">
									</div>
								</div>
								
								<div>
									<h4>Jane Doe</h4>
									
									<p>Sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation.</p>
									
									<ul id="practitioner-action">
										<li>
											<div><i class="fa fa-envelope"></i></div>
											
											<a>Message Practitioner&nbsp;&nbsp;&nbsp;</a>
										</li>
										
										<li>
											<div><i class="fa fa-bookmark"></i></div>
											
											<a>Bookmark</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="small-12 text-center">
					<a class="button">More Results</a>
				</div>
			</div>
		</section>
		
		<section class="search-result-container">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>11 km - 25 km</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12">
					<h3 class="text-center">No Practitioner Found</h3>
				</div>
			</div>
		</section>
		
		<section class="search-result-container">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>26 km - 50 km</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12">
					<h3 class="text-center">No Practitioner Found</h3>
				</div>
			</div>
		</section>
		
		<section class="search-result-container">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>51 km - 100 km</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12">
					<h3 class="text-center">No Practitioner Found</h3>
				</div>
			</div>
		</section>
		
		<section class="search-result-container">
			<header>
				<div class="row">
					<div class="columns small-12">
						<h2>
							<span>over 100 km</span>
						</h2>
					</div>
				</div>
			</header>
			
			<div class="row">
				<div class="columns small-12">
					<h3 class="text-center">No Practitioner Found</h3>
				</div>
			</div>
		</section>

<?php get_footer(); ?>