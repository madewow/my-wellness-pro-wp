<?php ?>

		<footer id="main-footer">
			<div class="row">
				<div class="columns small-6 medium-3">
					<h3>Directory</h3>
					
					<ul class="navigation-footer">
						<li>
							<a>Directory Link</a>
						</li>
						
						<li>
							<a>Directory Link</a>
						</li>
						
						<li>
							<a>Directory Link</a>
						</li>
					</ul>
				</div>
				
				<div class="columns small-6 medium-3">
					<h3>Business</h3>
					
					<ul class="navigation-footer">
						<li>
							<a>Business Link</a>
						</li>
						
						<li>
							<a>Business Link</a>
						</li>
						
						<li>
							<a>Business Link</a>
						</li>
					</ul>
				</div>
				
				<div class="columns small-12 medium-4">
					<h3>Contact</h3>
					
					<p>Get in touch</p>
					
					<h3>Social Media</h3>
					
					<ul id="social-media-navigation">
						<li>
							<a><i class="fa fa-facebook"></i></a>
						</li>
						
						<li>
							<a><i class="fa fa-twitter"></i></a>
						</li>
						
						<li>
							<a><i class="fa fa-instagram"></i></a>
						</li>
					</ul>
				</div>
				
				<div class="columns small-12 medium-2">
					<a class="logo"></a>
				</div>
			</div>
		</footer>
		
	    <?php wp_footer(); ?>
	    
	    <?php if (get_page_template_slug( $post_id ) == "search-result-template.php") { ?> 
	    <script>
		    var user_coordinate = new google.maps.LatLng(-6.285371, 106.831589);
		    
		    var markers = [[-6.2579708,106.8083432], [-6.2805394,106.8290841], [-6.2445432,106.8006869], [-6.2533242,106.80143]]
		    
		    // get visitor current location
		    function getLocation() {
			    if (navigator.geolocation) {
			       	navigator.geolocation.getCurrentPosition(show_position);
			        
			    } else {
			        x.innerHTML = "Geolocation is not supported by this browser.";
			    }
			}
		    
		    function show_position(position){
			    user_coordinate = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			    //console.log(position);
		    }
		    
			jQuery(document).on('ready', function() {
			    //getLocation();
			    
				_initialize_gmap('map', user_coordinate, null, markers);
			});    
		</script>
		
	    <?php } else if (get_page_template_slug( $post_id ) == "practitioner-template.php") { ?>
	    <script>
		    var practitioner_coordinate = new google.maps.LatLng(-6.252399,106.806007);
		    var user_coordinate = new google.maps.LatLng(-6.285371, 106.831589);
		    
		    // get visitor current location
		    function getLocation() {
			    if (navigator.geolocation) {
			       	navigator.geolocation.getCurrentPosition(show_position);
			        
			    } else {
			        x.innerHTML = "Geolocation is not supported by this browser.";
			    }
			}
		    
		    function show_position(position){
			    user_coordinate = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			    //console.log(position);
		    }
		    
		    
		    jQuery(document).on('ready', function(){
			    //getLocation();
			    
				_initialize_gmap('map', user_coordinate, practitioner_coordinate, null);
			});
		</script>
	    <?php } ?>
  	</body>
</html>