var map_style = [{"featureType":"landscape","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"stylers":[{"hue":"#00aaff"},{"saturation":-100},{"gamma":2.15},{"lightness":12}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"lightness":24}]},{"featureType":"road","elementType":"geometry","stylers":[{"lightness":57}]}];

function _initialize_gmap(_element, origin, destination, markers) {
		var bounds = new google.maps.LatLngBounds();
		var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer;
        var service = new google.maps.DistanceMatrixService();
        
	    var options = {
	        center: origin,
	        zoom: 11,
	        scrollwheel: false,
			navigationControl: false,
			mapTypeControl: false,
			scaleControl: false,
			draggable: false,
	        mapTypeId: google.maps.MapTypeId.ROADMAP,
	        styles: map_style };
	
	    var map = new google.maps.Map(document.getElementById(_element),  options);
	    
	    if (destination) {
		    directionsDisplay.setMap(map);
		    
		    _calculat_display_route(directionsService, directionsDisplay, origin, destination);
		    
		    _calculate_distance(service, origin, destination);
	    }
	    
	    if (markers) {
		    for( i = 0; i < markers.length; i++ ) {
		        var position = new google.maps.LatLng(markers[i][0], markers[i][1]);
		        
		        bounds.extend(position);
		        marker = new google.maps.Marker({
		            position: position,
		            map: map,
		        });
		    }
	    }
	}
	
function _calculat_display_route(directionsService, directionsDisplay, origin, destination) {
        directionsService.route({
         	origin: '"' + origin.lat() + ',' + origin.lng() + '"',
		 	destination: '"' + destination.lat() + ',' + destination.lng() + '"',
		 	travelMode: 'DRIVING'
		 	
        }, function(response, status) {
          	if (status === 'OK') {
            	directionsDisplay.setDirections(response);
            	
          	} else {
            	window.alert('Directions request failed due to ' + status);
          	}
        });
      }

function _calculate_distance_callback(response, status) {
	//console.log(response.rows[0].elements[0].distance.text);
	jquery('#distance').text(response.rows[0].elements[0].distance.text);
}

function _calculate_distance(service, origin, destination) {
		service.getDistanceMatrix(
		{
			origins: [origin],
			destinations: [destination],
			travelMode: 'DRIVING',
			avoidHighways: false,
			avoidTolls: true,
		}, _calculate_distance_callback);
	}

$(document).foundation();